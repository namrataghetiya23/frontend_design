<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ViewController extends Controller
{
    public function login()
    {
    	return view('page.login');
    }

    public function myProfile()
    {
    	return view('page.myProfile');
    }

    public function register()
    {
    	return view('page.register');
    }

    public function userType()
    {
    	return view('page.userType');
    }

    public function verifyNumber()
    {
    	return view('page.verifyNumber');
    }
    public function editProfile()
    {
        return view('page.editProfile'); 
    }
    public function BookingRequests()
    {
        return view('page.BookingRequests');
    }
    public function BookedCourses()
    {
        return view('page.BookedCourses');
    }
    public function TeacherProfile()
    {
        return view('page.TeacherProfile');
    }
    public function PaymentHistory()
    {
        return view('page.PaymentHistory');
    }
    public function SubjectDetail()
    {
        return view('page.SubjectDetail');
    }
    public function SessionDetail()
    {
        return view('page.SessionDetail');
    }
    public function SessionDetail2()
    {
        return view('page.SessionDetail2');
    }
     public function AccountSettings()
    {
        return view('page.AccountSettings');
    }

}
