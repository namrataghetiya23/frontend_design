@extends('main')
@section('content')
<section class="contain_wapper" id="contain_wapper">
   <div class="home_wapper">
      <div class="inner-banner">
         <div class="container">
            <div class="banner-content">
               <h1>Account Settings</h1>
            </div>
         </div>
      </div>
      <div class="account-settings">
         <form>
            <h2>Change Details</h2>
             <div class="form-group">
                <label for="email">Email ID *</label>
                <input type="email" class="form-control" id="email" placeholder="Enter Email ID">
              </div>
         </form>
      </div>
   </div>
</section>
@stop