@extends('main')
@section('content')
<section class="contain_wapper" id="contain_wapper">
   <div class="home_wapper">
      <div class="inner-banner">
         <div class="container">
            <div class="banner-content">
               <h1>My Booked courses</h1>
            </div>
         </div>
      </div>
      <div class="booking-section">
      <div class="container">
         <div class="booing-header">
            <h2>My Booked courses</h2>
         </div>
         <div class="booing-courses">
            <h2>Upcoming courses</h2>
         </div>
         <div class="row">
            <div class="col-md-4">
               <div class="book-box">
                  <div class="book-img">
                     <img src="images/book-img.png" alt="Book" class="img-fluid">
                     <span class="book-label">Web Design</span>
                  </div>
                  <div class="book-description">
                     <h4>Graphics Design Bootcamp: Photoshop, illustrator, InDesign</h4>
                     <label>Course SubTitle</label>
                     <div class="rating">
                        <em>5.0</em>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                     </div>
                     <ul>
                        <li>Teacher name : <strong>Name</strong></li>
                        <li>Age Group : <strong>Age</strong></li>
                        <li>Starts Date: <strong>04:10 PM</strong></li>
                        <li>Fees : <strong>$149</strong></li>
                     </ul>
                     <button class="btn btn-dark-orange">View Details</button>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="book-box">
                  <div class="book-img">
                     <img src="images/book-img.png" alt="Book" class="img-fluid">
                     <span class="book-label">Web Design</span>
                  </div>
                  <div class="book-description">
                     <h4>Graphics Design Bootcamp: Photoshop, illustrator, InDesign</h4>
                     <label>Course SubTitle</label>
                     <div class="rating">
                        <em>5.0</em>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                     </div>
                     <ul>
                        <li>Teacher name : <strong>Name</strong></li>
                        <li>Age Group : <strong>Age</strong></li>
                        <li>Starts Date: <strong>04:10 PM</strong></li>
                        <li>Fees : <strong>$149</strong></li>
                     </ul>
                     <button class="btn btn-dark-orange">View Details</button>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="book-box">
                  <div class="book-img">
                     <img src="images/book-img.png" alt="Book" class="img-fluid">
                     <span class="book-label">Web Design</span>
                  </div>
                  <div class="book-description">
                     <h4>Graphics Design Bootcamp: Photoshop, illustrator, InDesign</h4>
                     <label>Course SubTitle</label>
                     <div class="rating">
                        <em>5.0</em>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                     </div>
                     <ul>
                        <li>Teacher name : <strong>Name</strong></li>
                        <li>Age Group : <strong>Age</strong></li>
                        <li>Starts Date: <strong>04:10 PM</strong></li>
                        <li>Fees : <strong>$149</strong></li>
                     </ul>
                     <button class="btn btn-dark-orange">View Details</button>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="book-box">
                  <div class="book-img">
                     <img src="images/book-img.png" alt="Book" class="img-fluid">
                     <span class="book-label">Web Design</span>
                  </div>
                  <div class="book-description">
                     <h4>Graphics Design Bootcamp: Photoshop, illustrator, InDesign</h4>
                     <label>Course SubTitle</label>
                     <div class="rating">
                        <em>5.0</em>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                     </div>
                     <ul>
                        <li>Teacher name : <strong>Name</strong></li>
                        <li>Age Group : <strong>Age</strong></li>
                        <li>Starts Date: <strong>04:10 PM</strong></li>
                        <li>Fees : <strong>$149</strong></li>
                     </ul>
                     <button class="btn btn-dark-orange">View Details</button>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="book-box">
                  <div class="book-img">
                     <img src="images/book-img.png" alt="Book" class="img-fluid">
                     <span class="book-label">Web Design</span>
                  </div>
                  <div class="book-description">
                     <h4>Graphics Design Bootcamp: Photoshop, illustrator, InDesign</h4>
                     <label>Course SubTitle</label>
                     <div class="rating">
                        <em>5.0</em>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                     </div>
                     <ul>
                        <li>Teacher name : <strong>Name</strong></li>
                        <li>Age Group : <strong>Age</strong></li>
                        <li>Starts Date: <strong>04:10 PM</strong></li>
                        <li>Fees : <strong>$149</strong></li>
                     </ul>
                     <button class="btn btn-dark-orange">View Details</button>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="book-box">
                  <div class="book-img">
                     <img src="images/book-img.png" alt="Book" class="img-fluid">
                     <span class="book-label">Web Design</span>
                  </div>
                  <div class="book-description">
                     <h4>Graphics Design Bootcamp: Photoshop, illustrator, InDesign</h4>
                     <label>Course SubTitle</label>
                     <div class="rating">
                        <em>5.0</em>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                     </div>
                     <ul>
                        <li>Teacher name : <strong>Name</strong></li>
                        <li>Age Group : <strong>Age</strong></li>
                        <li>Starts Date: <strong>04:10 PM</strong></li>
                        <li>Fees : <strong>$149</strong></li>
                     </ul>
                     <button class="btn btn-dark-orange">View Details</button>
                  </div>
               </div>
            </div>
         </div>
         <div class="booing-courses">
            <h2>Completed courses</h2>
         </div>
         <div class="row">
            <div class="col-md-4">
               <div class="book-box">
                  <div class="book-img">
                     <img src="images/book-img.png" alt="Book" class="img-fluid">
                     <span class="book-label">Web Design</span>
                  </div>
                  <div class="book-description">
                     <h4>Graphics Design Bootcamp: Photoshop, illustrator, InDesign</h4>
                     <label>Course SubTitle</label>
                     <div class="rating">
                        <em>5.0</em>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                     </div>
                     <ul>
                        <li>Teacher name : <strong>Name</strong></li>
                        <li>Age Group : <strong>Age</strong></li>
                        <li>Starts Date: <strong>04:10 PM</strong></li>
                        <li>Fees : <strong>$149</strong></li>
                     </ul>
                     <button class="btn btn-dark-orange">View Details</button>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="book-box">
                  <div class="book-img">
                     <img src="images/book-img.png" alt="Book" class="img-fluid">
                     <span class="book-label">Web Design</span>
                  </div>
                  <div class="book-description">
                     <h4>Graphics Design Bootcamp: Photoshop, illustrator, InDesign</h4>
                     <label>Course SubTitle</label>
                     <div class="rating">
                        <em>5.0</em>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                     </div>
                     <ul>
                        <li>Teacher name : <strong>Name</strong></li>
                        <li>Age Group : <strong>Age</strong></li>
                        <li>Starts Date: <strong>04:10 PM</strong></li>
                        <li>Fees : <strong>$149</strong></li>
                     </ul>
                     <button class="btn btn-dark-orange">View Details</button>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="book-box">
                  <div class="book-img">
                     <img src="images/book-img.png" alt="Book" class="img-fluid">
                     <span class="book-label">Web Design</span>
                  </div>
                  <div class="book-description">
                     <h4>Graphics Design Bootcamp: Photoshop, illustrator, InDesign</h4>
                     <label>Course SubTitle</label>
                     <div class="rating">
                        <em>5.0</em>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                     </div>
                     <ul>
                        <li>Teacher name : <strong>Name</strong></li>
                        <li>Age Group : <strong>Age</strong></li>
                        <li>Starts Date: <strong>04:10 PM</strong></li>
                        <li>Fees : <strong>$149</strong></li>
                     </ul>
                     <button class="btn btn-dark-orange">View Details</button>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="book-box">
                  <div class="book-img">
                     <img src="images/book-img.png" alt="Book" class="img-fluid">
                     <span class="book-label">Web Design</span>
                  </div>
                  <div class="book-description">
                     <h4>Graphics Design Bootcamp: Photoshop, illustrator, InDesign</h4>
                     <label>Course SubTitle</label>
                     <div class="rating">
                        <em>5.0</em>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                     </div>
                     <ul>
                        <li>Teacher name : <strong>Name</strong></li>
                        <li>Age Group : <strong>Age</strong></li>
                        <li>Starts Date: <strong>04:10 PM</strong></li>
                        <li>Fees : <strong>$149</strong></li>
                     </ul>
                     <button class="btn btn-dark-orange">View Details</button>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="book-box">
                  <div class="book-img">
                     <img src="images/book-img.png" alt="Book" class="img-fluid">
                     <span class="book-label">Web Design</span>
                  </div>
                  <div class="book-description">
                     <h4>Graphics Design Bootcamp: Photoshop, illustrator, InDesign</h4>
                     <label>Course SubTitle</label>
                     <div class="rating">
                        <em>5.0</em>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                     </div>
                     <ul>
                        <li>Teacher name : <strong>Name</strong></li>
                        <li>Age Group : <strong>Age</strong></li>
                        <li>Starts Date: <strong>04:10 PM</strong></li>
                        <li>Fees : <strong>$149</strong></li>
                     </ul>
                     <button class="btn btn-dark-orange">View Details</button>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="book-box">
                  <div class="book-img">
                     <img src="images/book-img.png" alt="Book" class="img-fluid">
                     <span class="book-label">Web Design</span>
                  </div>
                  <div class="book-description">
                     <h4>Graphics Design Bootcamp: Photoshop, illustrator, InDesign</h4>
                     <label>Course SubTitle</label>
                     <div class="rating">
                        <em>5.0</em>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                     </div>
                     <ul>
                        <li>Teacher name : <strong>Name</strong></li>
                        <li>Age Group : <strong>Age</strong></li>
                        <li>Starts Date: <strong>04:10 PM</strong></li>
                        <li>Fees : <strong>$149</strong></li>
                     </ul>
                     <button class="btn btn-dark-orange">View Details</button>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
   </div>
</section>
@stop