@extends('main')
@section('content')
<section class="contain_wapper" id="contain_wapper">
   <div class="home_wapper">
      <div class="inner-banner">
         <div class="container">
            <div class="banner-content">
               <h1>My Booking Requests</h1>
            </div>
         </div>
      </div>
      <div class="booking-section">
      <div class="container">
         <div class="booing-header">
            <h2>Requested Tutors</h2>
         </div>
         <div class="row">
            <div class="col-md-4">
               <div class="book-box">
                  <div class="book-img">
                     <img src="images/book-img.png" alt="Book" class="img-fluid">
                     <span class="book-label">Web Design</span>
                  </div>
                  <div class="book-description">
                     <h4>Graphics Design Bootcamp: Photoshop, illustrator, InDesign</h4>
                     <label>Course SubTitle</label>
                     <div class="rating">
                        <em>5.0</em>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                     </div>
                     <ul>
                        <li>Duration: <strong>2 Hours</strong></li>
                        <li>Session Date and Time: <strong>24 june 2021</strong></li>
                        <li>Subject Type: <strong>Group</strong></li>
                        <li>Request Status: <strong class="color-green">Accepted</strong></li>
                        <li>Requested Date: <strong>24 june 2021</strong></li>
                        <li>seats available : <strong>3 Out of 5</strong></li>
                        <li>Teacher name : <strong>Name</strong></li>
                        <li>Age Group : <strong>Age</strong></li>
                        <li>Fees : <strong>$149</strong></li>
                     </ul>
                     <button class="btn btn-orange">Cancel Request</button>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="book-box">
                  <div class="book-img">
                     <img src="images/book-img.png" alt="Book" class="img-fluid">
                     <span class="book-label">Web Design</span>
                  </div>
                  <div class="book-description">
                     <h4>Graphics Design Bootcamp: Photoshop, illustrator, InDesign</h4>
                     <label>Course SubTitle</label>
                     <div class="rating">
                        <em>5.0</em>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                     </div>
                     <ul>
                        <li>Duration: <strong>2 Hours</strong></li>
                        <li>Session Date and Time: <strong>24 june 2021</strong></li>
                        <li>Subject Type: <strong>Group</strong></li>
                        <li>Request Status: <strong class="color-green">Accepted</strong></li>
                        <li>Requested Date: <strong>24 june 2021</strong></li>
                        <li>seats available : <strong>3 Out of 5</strong></li>
                        <li>Teacher name : <strong>Name</strong></li>
                        <li>Age Group : <strong>Age</strong></li>
                        <li>Fees : <strong>$149</strong></li>
                     </ul>
                     <button class="btn btn-orange">Cancel Request</button>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="book-box">
                  <div class="book-img">
                     <img src="images/book-img.png" alt="Book" class="img-fluid">
                     <span class="book-label">Web Design</span>
                  </div>
                  <div class="book-description">
                     <h4>Graphics Design Bootcamp: Photoshop, illustrator, InDesign</h4>
                     <label>Course SubTitle</label>
                     <div class="rating">
                        <em>5.0</em>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                     </div>
                     <ul>
                        <li>Duration: <strong>2 Hours</strong></li>
                        <li>Session Date and Time: <strong>24 june 2021</strong></li>
                        <li>Subject Type: <strong>Group</strong></li>
                        <li>Request Status: <strong class="color-green">Accepted</strong></li>
                        <li>Requested Date: <strong>24 june 2021</strong></li>
                        <li>seats available : <strong>3 Out of 5</strong></li>
                        <li>Teacher name : <strong>Name</strong></li>
                        <li>Age Group : <strong>Age</strong></li>
                        <li>Fees : <strong>$149</strong></li>
                     </ul>
                     <button class="btn btn-orange">Cancel Request</button>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="book-box">
                  <div class="book-img">
                     <img src="images/book-img.png" alt="Book" class="img-fluid">
                     <span class="book-label">Web Design</span>
                  </div>
                  <div class="book-description">
                     <h4>Graphics Design Bootcamp: Photoshop, illustrator, InDesign</h4>
                     <label>Course SubTitle</label>
                     <div class="rating">
                        <em>5.0</em>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                     </div>
                     <ul>
                        <li>Duration: <strong>2 Hours</strong></li>
                        <li>Session Date and Time: <strong>24 june 2021</strong></li>
                        <li>Subject Type: <strong>Group</strong></li>
                        <li>Request Status: <strong class="color-green">Accepted</strong></li>
                        <li>Requested Date: <strong>24 june 2021</strong></li>
                        <li>seats available : <strong>3 Out of 5</strong></li>
                        <li>Teacher name : <strong>Name</strong></li>
                        <li>Age Group : <strong>Age</strong></li>
                        <li>Fees : <strong>$149</strong></li>
                     </ul>
                     <button class="btn btn-orange">Cancel Request</button>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="book-box">
                  <div class="book-img">
                     <img src="images/book-img.png" alt="Book" class="img-fluid">
                     <span class="book-label">Web Design</span>
                  </div>
                  <div class="book-description">
                     <h4>Graphics Design Bootcamp: Photoshop, illustrator, InDesign</h4>
                     <label>Course SubTitle</label>
                     <div class="rating">
                        <em>5.0</em>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                     </div>
                     <ul>
                        <li>Duration: <strong>2 Hours</strong></li>
                        <li>Session Date and Time: <strong>24 june 2021</strong></li>
                        <li>Subject Type: <strong>Group</strong></li>
                        <li>Request Status: <strong class="color-green">Accepted</strong></li>
                        <li>Requested Date: <strong>24 june 2021</strong></li>
                        <li>seats available : <strong>3 Out of 5</strong></li>
                        <li>Teacher name : <strong>Name</strong></li>
                        <li>Age Group : <strong>Age</strong></li>
                        <li>Fees : <strong>$149</strong></li>
                     </ul>
                     <button class="btn btn-orange">Cancel Request</button>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="book-box">
                  <div class="book-img">
                     <img src="images/book-img.png" alt="Book" class="img-fluid">
                     <span class="book-label">Web Design</span>
                  </div>
                  <div class="book-description">
                     <h4>Graphics Design Bootcamp: Photoshop, illustrator, InDesign</h4>
                     <label>Course SubTitle</label>
                     <div class="rating">
                        <em>5.0</em>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                     </div>
                     <ul>
                        <li>Duration: <strong>2 Hours</strong></li>
                        <li>Session Date and Time: <strong>24 june 2021</strong></li>
                        <li>Subject Type: <strong>Group</strong></li>
                        <li>Request Status: <strong class="color-green">Accepted</strong></li>
                        <li>Requested Date: <strong>24 june 2021</strong></li>
                        <li>seats available : <strong>3 Out of 5</strong></li>
                        <li>Teacher name : <strong>Name</strong></li>
                        <li>Age Group : <strong>Age</strong></li>
                        <li>Fees : <strong>$149</strong></li>
                     </ul>
                     <button class="btn btn-orange">Cancel Request</button>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="book-box">
                  <div class="book-img">
                     <img src="images/book-img.png" alt="Book" class="img-fluid">
                     <span class="book-label">Web Design</span>
                  </div>
                  <div class="book-description">
                     <h4>Graphics Design Bootcamp: Photoshop, illustrator, InDesign</h4>
                     <label>Course SubTitle</label>
                     <div class="rating">
                        <em>5.0</em>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                     </div>
                     <ul>
                        <li>Duration: <strong>2 Hours</strong></li>
                        <li>Session Date and Time: <strong>24 june 2021</strong></li>
                        <li>Subject Type: <strong>Group</strong></li>
                        <li>Request Status: <strong class="color-green">Accepted</strong></li>
                        <li>Requested Date: <strong>24 june 2021</strong></li>
                        <li>seats available : <strong>3 Out of 5</strong></li>
                        <li>Teacher name : <strong>Name</strong></li>
                        <li>Age Group : <strong>Age</strong></li>
                        <li>Fees : <strong>$149</strong></li>
                     </ul>
                     <button class="btn btn-orange">Cancel Request</button>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="book-box">
                  <div class="book-img">
                     <img src="images/book-img.png" alt="Book" class="img-fluid">
                     <span class="book-label">Web Design</span>
                  </div>
                  <div class="book-description">
                     <h4>Graphics Design Bootcamp: Photoshop, illustrator, InDesign</h4>
                     <label>Course SubTitle</label>
                     <div class="rating">
                        <em>5.0</em>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                     </div>
                     <ul>
                        <li>Duration: <strong>2 Hours</strong></li>
                        <li>Session Date and Time: <strong>24 june 2021</strong></li>
                        <li>Subject Type: <strong>Group</strong></li>
                        <li>Request Status: <strong class="color-green">Accepted</strong></li>
                        <li>Requested Date: <strong>24 june 2021</strong></li>
                        <li>seats available : <strong>3 Out of 5</strong></li>
                        <li>Teacher name : <strong>Name</strong></li>
                        <li>Age Group : <strong>Age</strong></li>
                        <li>Fees : <strong>$149</strong></li>
                     </ul>
                     <button class="btn btn-orange">Cancel Request</button>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="book-box">
                  <div class="book-img">
                     <img src="images/book-img.png" alt="Book" class="img-fluid">
                     <span class="book-label">Web Design</span>
                  </div>
                  <div class="book-description">
                     <h4>Graphics Design Bootcamp: Photoshop, illustrator, InDesign</h4>
                     <label>Course SubTitle</label>
                     <div class="rating">
                        <em>5.0</em>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                        <i class='bx bxs-star' ></i>
                     </div>
                     <ul>
                        <li>Duration: <strong>2 Hours</strong></li>
                        <li>Session Date and Time: <strong>24 june 2021</strong></li>
                        <li>Subject Type: <strong>Group</strong></li>
                        <li>Request Status: <strong class="color-green">Accepted</strong></li>
                        <li>Requested Date: <strong>24 june 2021</strong></li>
                        <li>seats available : <strong>3 Out of 5</strong></li>
                        <li>Teacher name : <strong>Name</strong></li>
                        <li>Age Group : <strong>Age</strong></li>
                        <li>Fees : <strong>$149</strong></li>
                     </ul>
                     <button class="btn btn-orange">Cancel Request</button>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
   </div>
</section>
@stop