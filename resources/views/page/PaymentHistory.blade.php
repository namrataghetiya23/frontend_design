@extends('main')
@section('content')
<section class="contain_wapper" id="contain_wapper">
   <div class="home_wapper">
      <div class="inner-banner">
         <div class="container">
            <div class="banner-content">
               <h1>Payment History</h1>
            </div>
         </div>
      </div>
      <div class="booking-section">
      <div class="container">
         <div class="booing-header">
            <h2>Payment History</h2>
         </div>
         <div class="table-responsive">
         <table class="table table-striped">
           <thead>
             <tr>
               <th scope="col">Transaction ID</th>
               <th scope="col">Transaction Date</th>
               <th scope="col">Course Name</th>
               <th scope="col">Tutor Name</th>
               <th scope="col">Transaction Amount</th>
             </tr>
           </thead>
           <tbody>
             <tr>
               <th>#01236547890</th>
               <td>21 june 2020</td>
               <td>Web Design</td>
               <td>Alex xendor</td>
               <td>$149.99</td>
             </tr>
             <tr>
               <th>#01236547890</th>
               <td>21 june 2020</td>
               <td>Web Design</td>
               <td>Alex xendor</td>
               <td>$149.99</td>
             </tr>
             <tr>
               <th>#01236547890</th>
               <td>21 june 2020</td>
               <td>Web Design</td>
               <td>Alex xendor</td>
               <td>$149.99</td>
             </tr>
             <tr>
               <th>#01236547890</th>
               <td>21 june 2020</td>
               <td>Web Design</td>
               <td>Alex xendor</td>
               <td>$149.99</td>
             </tr>
             <tr>
               <th>#01236547890</th>
               <td>21 june 2020</td>
               <td>Web Design</td>
               <td>Alex xendor</td>
               <td>$149.99</td>
             </tr>
             <tr>
               <th>#01236547890</th>
               <td>21 june 2020</td>
               <td>Web Design</td>
               <td>Alex xendor</td>
               <td>$149.99</td>
             </tr>
             <tr>
               <th>#01236547890</th>
               <td>21 june 2020</td>
               <td>Web Design</td>
               <td>Alex xendor</td>
               <td>$149.99</td>
             </tr>
             <tr>
               <th>#01236547890</th>
               <td>21 june 2020</td>
               <td>Web Design</td>
               <td>Alex xendor</td>
               <td>$149.99</td>
             </tr>
             <tr>
               <th>#01236547890</th>
               <td>21 june 2020</td>
               <td>Web Design</td>
               <td>Alex xendor</td>
               <td>$149.99</td>
             </tr>
             <tr>
               <th>#01236547890</th>
               <td>21 june 2020</td>
               <td>Web Design</td>
               <td>Alex xendor</td>
               <td>$149.99</td>
             </tr>
             <tr>
               <th>#01236547890</th>
               <td>21 june 2020</td>
               <td>Web Design</td>
               <td>Alex xendor</td>
               <td>$149.99</td>
             </tr>
             <tr>
               <th>#01236547890</th>
               <td>21 june 2020</td>
               <td>Web Design</td>
               <td>Alex xendor</td>
               <td>$149.99</td>
             </tr>
             <tr>
               <th>#01236547890</th>
               <td>21 june 2020</td>
               <td>Web Design</td>
               <td>Alex xendor</td>
               <td>$149.99</td>
             </tr>
             <tr>
               <th>#01236547890</th>
               <td>21 june 2020</td>
               <td>Web Design</td>
               <td>Alex xendor</td>
               <td>$149.99</td>
             </tr>
             <tr>
               <th>#01236547890</th>
               <td>21 june 2020</td>
               <td>Web Design</td>
               <td>Alex xendor</td>
               <td>$149.99</td>
             </tr>
             <tr>
               <th>#01236547890</th>
               <td>21 june 2020</td>
               <td>Web Design</td>
               <td>Alex xendor</td>
               <td>$149.99</td>
             </tr>
             <tr>
               <th>#01236547890</th>
               <td>21 june 2020</td>
               <td>Web Design</td>
               <td>Alex xendor</td>
               <td>$149.99</td>
             </tr>
             <tr>
               <th>#01236547890</th>
               <td>21 june 2020</td>
               <td>Web Design</td>
               <td>Alex xendor</td>
               <td>$149.99</td>
             </tr>
             <tr>
               <th>#01236547890</th>
               <td>21 june 2020</td>
               <td>Web Design</td>
               <td>Alex xendor</td>
               <td>$149.99</td>
             </tr>
             
           </tbody>
         </table>
      </div>
      </div>
      </div>
   </div>
</section>
@stop