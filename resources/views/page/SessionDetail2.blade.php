@extends('main')
@section('content')
<section class="contain_wapper" id="contain_wapper">
   <div class="home_wapper">
      <div class="inner-banner banner-images">
         <img src="images/banner-imgg.png" alt="bannner images">
         <div class="container">
            <!-- <div class="banner-content">
               <h1>Teacher Name</h1>
            </div> -->
         </div>
      </div>
      <div class="user-profile">
         <div class="container">
            <div class="user-title mb-20">
               <span class="orange-text f-40">Graphics Design Bootcamp: Photoshop, illustrator, InDesign</span>
            </div>
            <div class="book-description"> 
            <div class="row">
               <div class="col-md-6">
                  <ul>
                  <li>Category : <strong class="bold">category</strong></li>
                  <li>Sub-category : <strong class="bold"> Sub-category</strong></li>
                  <li>Subject Type : <strong class="bold">One to One</strong></li>
                  <li>Session Date and Time : <strong class="bold">Date and Time</strong></li>
                  <li>Grade : <strong class="bold">A+</strong></li>
                  <li>Assessment Status : <strong class="bold">Pending</strong> <span class="orange-text"><box-icon name='download'></box-icon> Download</span></li>
               </ul>
               </div>
               <div class="col-md-6">
                  <ul>
                  <li>Subject Type : <strong class="bold">Group</strong></li>
                  <li>Tutor Name : <strong class="bold">Name</strong></li>
                  <li>
                     <div class="rating">
                        <em>5.0</em>
                        <i class="bx bxs-star"></i>
                        <i class="bx bxs-star"></i>
                        <i class="bx bxs-star"></i>
                        <i class="bx bxs-star"></i>
                        <i class="bx bxs-star"></i>
                     </div>
                  </li>
                  <li>Recived Review : <strong class="bold">Review Text</strong></li>
               </ul>
               </div>
            </div>                  
            </div>
            <div class="course-content">
               <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum magnam excepturi doloribus cum blanditiis, fugit enim culpa voluptatum maxime non nisi labore doloremque consequuntur rem ipsam, aliquam dolores ipsa recusandae! Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum magnam excepturi doloribus cum blanditiis, fugit enim culpa voluptatum maxime non nisi labore doloremque consequuntur rem ipsam, aliquam dolores ipsa recusandae! Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum magnam excepturi doloribus cum blanditiis, fugit enim culpa voluptatum maxime non nisi labore doloremque consequuntur rem ipsam, aliquam dolores ipsa recusandae!</p>
               <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum magnam excepturi doloribus cum blanditiis, fugit enim culpa voluptatum maxime non nisi labore doloremque consequuntur rem ipsam, aliquam dolores ipsa recusandae! Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum magnam excepturi doloribus cum blanditiis, fugit enim culpa voluptatum maxime non nisi labore doloremque consequuntur rem ipsam, aliquam dolores ipsa recusandae! Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum magnam excepturi doloribus cum blanditiis, fugit enim culpa voluptatum maxime non nisi labore doloremque consequuntur rem ipsam, aliquam dolores ipsa recusandae!</p> <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum magnam excepturi doloribus cum blanditiis, fugit enim culpa voluptatum maxime non nisi labore doloremque consequuntur rem ipsam, aliquam dolores ipsa recusandae! Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum magnam excepturi doloribus cum blanditiis, fugit enim culpa voluptatum maxime non nisi labore doloremque consequuntur rem ipsam, aliquam dolores ipsa recusandae! Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum magnam excepturi doloribus cum blanditiis, fugit enim culpa voluptatum maxime non nisi labore doloremque consequuntur rem ipsam, aliquam dolores ipsa recusandae!</p> <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum magnam excepturi doloribus cum blanditiis, fugit enim culpa voluptatum maxime non nisi labore doloremque consequuntur rem ipsam, aliquam dolores ipsa recusandae! Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum magnam excepturi doloribus cum blanditiis, fugit enim culpa voluptatum maxime non nisi labore doloremque consequuntur rem ipsam, aliquam dolores ipsa recusandae! Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum magnam excepturi doloribus cum blanditiis, fugit enim culpa voluptatum maxime non nisi labore doloremque consequuntur rem ipsam, aliquam dolores ipsa recusandae!</p> <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum magnam excepturi doloribus cum blanditiis, fugit enim culpa voluptatum maxime non nisi labore doloremque consequuntur rem ipsam, aliquam dolores ipsa recusandae! Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum magnam excepturi doloribus cum blanditiis, fugit enim culpa voluptatum maxime non nisi labore doloremque consequuntur rem ipsam, aliquam dolores ipsa recusandae! Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum magnam excepturi doloribus cum blanditiis, fugit enim culpa voluptatum maxime non nisi labore doloremque consequuntur rem ipsam, aliquam dolores ipsa recusandae!</p>
            </div>
         
            <div class="batch-section">
              <div class="batch-title">
               <h3><span class="orange-text">Batch:1 </span>Thu, July 22nd</h3>
              </div>
              <ul>
               <li><span>Thu, July 22nd 1:00 PM - 2:00 PM</span> <strong>1 Seat Remaining Out of 10</strong> <button class="btn btn-dark-orange small-btn">Start Now</button></li>
               <li><span>Thu, July 22nd 1:00 PM - 2:00 PM</span> <strong>1 Seat Remaining Out of 10</strong> <button class="btn btn-dark-orange small-btn">Start Now</button></li>
               <li><span>Thu, July 22nd 1:00 PM - 2:00 PM</span> <strong>1 Seat Remaining Out of 10</strong> <button class="btn btn-dark-orange small-btn">Start Now</button></li>
               <li><span>Thu, July 22nd 1:00 PM - 2:00 PM</span> <strong>1 Seat Remaining Out of 10</strong> <button class="btn btn-dark-orange small-btn">Start Now</button></li>
              </ul>
            </div>
         </div>
      </div>
   </div>
</section>
@stop