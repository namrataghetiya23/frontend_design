@extends('main')
@section('content')
<section class="contain_wapper" id="contain_wapper">
   <div class="home_wapper">
      <div class="inner-banner banner-images">
         <img src="images/banner-imgg.png" alt="bannner images">
         <div class="container">
            <!-- <div class="banner-content">
               <h1>Teacher Name</h1>
            </div> -->
         </div>
      </div>
      <div class="user-profile">
         <div class="container">
            <div class="user-title mb-20">
               <span class="orange-text f-40">Graphics Design Bootcamp: Photoshop, illustrator, InDesign</span>
            </div>
            <div class="book-description"> 
            <div class="row">
               <div class="col-md-6">
                  <ul>
                  <li>Subject Type : <strong class="bold">Group</strong></li>
                  <li>Purchased Date : <strong class="bold">21 june 2021</strong></li>
                  <li>Expiration Date : <strong class="bold">24 june 2021</strong></li>
                  <li>seats available : <strong class="bold">3 Out of 5</strong></li>
               </ul>
               </div>
               <div class="col-md-6">
                  <ul>
                  <li>Subject Type : <strong class="bold">Group</strong></li>
                  <li>Tutor Name : <strong class="bold">Name</strong></li>
                  <li>
                     <div class="rating">
                        <em>5.0</em>
                        <i class="bx bxs-star"></i>
                        <i class="bx bxs-star"></i>
                        <i class="bx bxs-star"></i>
                        <i class="bx bxs-star"></i>
                        <i class="bx bxs-star"></i>
                     </div>
                  </li>
               </ul>
               </div>
            </div>                  
            </div>
            <div class="course-content">
               <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum magnam excepturi doloribus cum blanditiis, fugit enim culpa voluptatum maxime non nisi labore doloremque consequuntur rem ipsam, aliquam dolores ipsa recusandae! Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum magnam excepturi doloribus cum blanditiis, fugit enim culpa voluptatum maxime non nisi labore doloremque consequuntur rem ipsam, aliquam dolores ipsa recusandae! Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum magnam excepturi doloribus cum blanditiis, fugit enim culpa voluptatum maxime non nisi labore doloremque consequuntur rem ipsam, aliquam dolores ipsa recusandae!</p>
               <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum magnam excepturi doloribus cum blanditiis, fugit enim culpa voluptatum maxime non nisi labore doloremque consequuntur rem ipsam, aliquam dolores ipsa recusandae! Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum magnam excepturi doloribus cum blanditiis, fugit enim culpa voluptatum maxime non nisi labore doloremque consequuntur rem ipsam, aliquam dolores ipsa recusandae! Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum magnam excepturi doloribus cum blanditiis, fugit enim culpa voluptatum maxime non nisi labore doloremque consequuntur rem ipsam, aliquam dolores ipsa recusandae!</p> <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum magnam excepturi doloribus cum blanditiis, fugit enim culpa voluptatum maxime non nisi labore doloremque consequuntur rem ipsam, aliquam dolores ipsa recusandae! Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum magnam excepturi doloribus cum blanditiis, fugit enim culpa voluptatum maxime non nisi labore doloremque consequuntur rem ipsam, aliquam dolores ipsa recusandae! Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum magnam excepturi doloribus cum blanditiis, fugit enim culpa voluptatum maxime non nisi labore doloremque consequuntur rem ipsam, aliquam dolores ipsa recusandae!</p> <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum magnam excepturi doloribus cum blanditiis, fugit enim culpa voluptatum maxime non nisi labore doloremque consequuntur rem ipsam, aliquam dolores ipsa recusandae! Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum magnam excepturi doloribus cum blanditiis, fugit enim culpa voluptatum maxime non nisi labore doloremque consequuntur rem ipsam, aliquam dolores ipsa recusandae! Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum magnam excepturi doloribus cum blanditiis, fugit enim culpa voluptatum maxime non nisi labore doloremque consequuntur rem ipsam, aliquam dolores ipsa recusandae!</p> <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum magnam excepturi doloribus cum blanditiis, fugit enim culpa voluptatum maxime non nisi labore doloremque consequuntur rem ipsam, aliquam dolores ipsa recusandae! Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum magnam excepturi doloribus cum blanditiis, fugit enim culpa voluptatum maxime non nisi labore doloremque consequuntur rem ipsam, aliquam dolores ipsa recusandae! Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum magnam excepturi doloribus cum blanditiis, fugit enim culpa voluptatum maxime non nisi labore doloremque consequuntur rem ipsam, aliquam dolores ipsa recusandae!</p>
            </div>
            <div class="button-section">
               <button class="btn btn-dark-orange small-btn">Book Now</button>
               <button class="btn btn-dark-orange small-btn">Request</button>
               <button class="btn btn-orange small-btn">Cancel Request</button>
            </div>
         </div>
      </div>
   </div>
</section>
@stop