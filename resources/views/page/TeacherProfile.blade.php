@extends('main')
@section('content')
<section class="contain_wapper" id="contain_wapper">
   <div class="home_wapper">
      <div class="inner-banner">
         <div class="container">
            <div class="banner-content">
               <h1>Teacher Name</h1>
            </div>
         </div>
      </div>
      <div class="user-profile">
         <div class="container">
            <div class="user-header">
               <img src="images/user-icon.png">
            </div>
            <div class="user-title">
               <span>Tutor Details</span>
            </div>
            <div class="user-datalist">
               <ul>
                  <li>
                     <strong>Tutor Name:</strong>
                     <span>First Name</span>
                  </li>
                  <li>
                     <strong>Tutor Rating:</strong>
                    <div class="rating">
                        <em>5.0</em>
                        <i class="bx bxs-star"></i>
                        <i class="bx bxs-star"></i>
                        <i class="bx bxs-star"></i>
                        <i class="bx bxs-star"></i>
                        <i class="bx bxs-star"></i>
                     </div>
                  </li>
                  <li>
                     <strong>School:</strong>
                     <span>School Name</span>
                  </li>
                  <li>
                     <strong>Contact No.:</strong>
                     <span>Contact No.</span>
                  </li>                  
                  <li>
                     <strong>City:</strong>
                     <span>City Name</span>
                  </li>
                  <li>
                     <strong>State:</strong>
                     <span>State: Name</span>
                  </li>
                  <li>
                     <strong>Country:</strong>
                     <span>Country Name</span>
                  </li>
                  <li class="full-width">
                     <strong>Tutor About :</strong>
                     <span>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Natus rerum, qui odio rem sint impedit, nesciunt provident at culpa</span>
                  </li>
                  <li class="full-width">
                     <strong>Description :</strong>
                     <span>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Natus rerum, qui odio rem sint impedit, nesciunt provident at culpa</span>
                  </li>
                  <li class="full-width">
                     <strong>Categories:</strong>
                     <span>Categories Name, Categories Name, Categories Name</span>
                  </li>
                  <li class="full-width">
                     <strong>Sub Categories:</strong>
                     <span>Sub Categories, Sub Categories, Sub Categories</span>
                  </li>
               </ul>
               <div class="sub-title">
                  <h2>Educational Qualification :</h2>
               </div>
               <ul>
                  <li>
                     <strong>Degree:</strong>
                     <span>Degree</span>
                  </li>
                  <li>
                     <strong>Year of Passing:</strong>
                     <span>Years</span>
                  </li>
                  <li>
                     <strong>Grades:</strong>
                     <span>Grades</span>
                  </li>
                  <li>
                     <strong>Institution Name:</strong>
                     <span>Institution Name</span>
                  </li>
               </ul>
               <div class="sub-title">
                  <h2>Educational Qualification :</h2>
               </div>
               <ul>
                  <li>
                     <strong>Organization Name:</strong>
                     <span>Organization Name</span>
                  </li>
                  <li>
                     <strong>Designation:</strong>
                     <span>Designation</span>
                  </li>
                  <li>
                     <strong>Year of Joining:</strong>
                     <span>Year</span>
                  </li>
                  <li>
                     <strong>Year of Leaving:</strong>
                     <span>Year</span>
                  </li>
                  <li>
                     <strong>Years of experience:</strong>
                     <span>Year</span>
                  </li>
               </ul>
               <div class="sub-title">
                  <h2>Subjects Offered :</h2>
               </div>
               <div class="row">
                  <div class="col-md-4">
                     <div class="book-box">
                        <div class="book-img">
                           <img src="images/book-img.png" alt="Book" class="img-fluid">
                           <span class="book-label">Web Design</span>
                        </div>
                        <div class="book-description">
                           <h4>Graphics Design Bootcamp: Photoshop, illustrator, InDesign</h4>
                           <label>Course SubTitle</label>
                           <div class="rating">
                              <em>5.0</em>
                              <i class="bx bxs-star"></i>
                              <i class="bx bxs-star"></i>
                              <i class="bx bxs-star"></i>
                              <i class="bx bxs-star"></i>
                              <i class="bx bxs-star"></i>
                           </div>
                           <ul>
                              <li>Teacher name : <strong>Name</strong></li>
                              <li>Age Group : <strong>Age</strong></li>
                              <li>Starts Date: <strong>04:10 PM</strong></li>
                              <li>Fees : <strong>$149</strong></li>
                           </ul>
                           <button class="btn btn-dark-orange">View Details</button>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="book-box">
                        <div class="book-img">
                           <img src="images/book-img.png" alt="Book" class="img-fluid">
                           <span class="book-label">Web Design</span>
                        </div>
                        <div class="book-description">
                           <h4>Graphics Design Bootcamp: Photoshop, illustrator, InDesign</h4>
                           <label>Course SubTitle</label>
                           <div class="rating">
                              <em>5.0</em>
                              <i class="bx bxs-star"></i>
                              <i class="bx bxs-star"></i>
                              <i class="bx bxs-star"></i>
                              <i class="bx bxs-star"></i>
                              <i class="bx bxs-star"></i>
                           </div>
                           <ul>
                              <li>Teacher name : <strong>Name</strong></li>
                              <li>Age Group : <strong>Age</strong></li>
                              <li>Starts Date: <strong>04:10 PM</strong></li>
                              <li>Fees : <strong>$149</strong></li>
                           </ul>
                           <button class="btn btn-dark-orange">View Details</button>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="book-box">
                        <div class="book-img">
                           <img src="images/book-img.png" alt="Book" class="img-fluid">
                           <span class="book-label">Web Design</span>
                        </div>
                        <div class="book-description">
                           <h4>Graphics Design Bootcamp: Photoshop, illustrator, InDesign</h4>
                           <label>Course SubTitle</label>
                           <div class="rating">
                              <em>5.0</em>
                              <i class="bx bxs-star"></i>
                              <i class="bx bxs-star"></i>
                              <i class="bx bxs-star"></i>
                              <i class="bx bxs-star"></i>
                              <i class="bx bxs-star"></i>
                           </div>
                           <ul>
                              <li>Teacher name : <strong>Name</strong></li>
                              <li>Age Group : <strong>Age</strong></li>
                              <li>Starts Date: <strong>04:10 PM</strong></li>
                              <li>Fees : <strong>$149</strong></li>
                           </ul>
                           <button class="btn btn-dark-orange">View Details</button>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="book-box">
                        <div class="book-img">
                           <img src="images/book-img.png" alt="Book" class="img-fluid">
                           <span class="book-label">Web Design</span>
                        </div>
                        <div class="book-description">
                           <h4>Graphics Design Bootcamp: Photoshop, illustrator, InDesign</h4>
                           <label>Course SubTitle</label>
                           <div class="rating">
                              <em>5.0</em>
                              <i class="bx bxs-star"></i>
                              <i class="bx bxs-star"></i>
                              <i class="bx bxs-star"></i>
                              <i class="bx bxs-star"></i>
                              <i class="bx bxs-star"></i>
                           </div>
                           <ul>
                              <li>Teacher name : <strong>Name</strong></li>
                              <li>Age Group : <strong>Age</strong></li>
                              <li>Starts Date: <strong>04:10 PM</strong></li>
                              <li>Fees : <strong>$149</strong></li>
                           </ul>
                           <button class="btn btn-dark-orange">View Details</button>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="book-box">
                        <div class="book-img">
                           <img src="images/book-img.png" alt="Book" class="img-fluid">
                           <span class="book-label">Web Design</span>
                        </div>
                        <div class="book-description">
                           <h4>Graphics Design Bootcamp: Photoshop, illustrator, InDesign</h4>
                           <label>Course SubTitle</label>
                           <div class="rating">
                              <em>5.0</em>
                              <i class="bx bxs-star"></i>
                              <i class="bx bxs-star"></i>
                              <i class="bx bxs-star"></i>
                              <i class="bx bxs-star"></i>
                              <i class="bx bxs-star"></i>
                           </div>
                           <ul>
                              <li>Teacher name : <strong>Name</strong></li>
                              <li>Age Group : <strong>Age</strong></li>
                              <li>Starts Date: <strong>04:10 PM</strong></li>
                              <li>Fees : <strong>$149</strong></li>
                           </ul>
                           <button class="btn btn-dark-orange">View Details</button>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="book-box">
                        <div class="book-img">
                           <img src="images/book-img.png" alt="Book" class="img-fluid">
                           <span class="book-label">Web Design</span>
                        </div>
                        <div class="book-description">
                           <h4>Graphics Design Bootcamp: Photoshop, illustrator, InDesign</h4>
                           <label>Course SubTitle</label>
                           <div class="rating">
                              <em>5.0</em>
                              <i class="bx bxs-star"></i>
                              <i class="bx bxs-star"></i>
                              <i class="bx bxs-star"></i>
                              <i class="bx bxs-star"></i>
                              <i class="bx bxs-star"></i>
                           </div>
                           <ul>
                              <li>Teacher name : <strong>Name</strong></li>
                              <li>Age Group : <strong>Age</strong></li>
                              <li>Starts Date: <strong>04:10 PM</strong></li>
                              <li>Fees : <strong>$149</strong></li>
                           </ul>
                           <button class="btn btn-dark-orange">View Details</button>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="sub-title">
                  <h2>Uploaded Documnet:</h2>
               </div>
               <div class="row">
                  <div class="col-md-4">
                     <div class="book-box">
                        <div class="book-img">
                           <img src="images/book-img.png" alt="Book" class="img-fluid">
                           <span class="book-label">Web Design</span>
                        </div>                        
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="book-box">
                        <div class="book-img">
                           <img src="images/book-img.png" alt="Book" class="img-fluid">
                           <span class="book-label">Web Design</span>
                        </div>                        
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="book-box">
                        <div class="book-img">
                           <img src="images/book-img.png" alt="Book" class="img-fluid">
                           <span class="book-label">Web Design</span>
                        </div>                        
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="book-box">
                        <div class="book-img">
                           <img src="images/book-img.png" alt="Book" class="img-fluid">
                           <span class="book-label">Web Design</span>
                        </div>                        
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="book-box">
                        <div class="book-img">
                           <img src="images/book-img.png" alt="Book" class="img-fluid">
                           <span class="book-label">Web Design</span>
                        </div>                        
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="book-box">
                        <div class="book-img">
                           <img src="images/book-img.png" alt="Book" class="img-fluid">
                           <span class="book-label">Web Design</span>
                        </div>                        
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
@stop