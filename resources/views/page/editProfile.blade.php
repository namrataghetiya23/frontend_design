@extends('main')
@section('content')
<section class="contain_wapper" id="contain_wapper">
   <div class="home_wapper">
      <div class="inner-banner">
         <div class="container">
            <div class="banner-content">
               <h1>Edit Profile</h1>
            </div>
         </div>
      </div>
      <div class="user-profile edit-profile">
         <div class="container">
            <div class="user-header">
            	<i class='bx bxs-edit-alt'></i>
               <img src="images/edit-icon.png">
            </div>
            <div class="user-title">
               <span>Student Details</span>
            </div>
            <div class="user-datalist">
            	<form>
               <ul>
                  <li>                    
                     <span>First Name</span>
                     <input type="text" placeholder="" name="name">
                  </li>
                  <li>
                     <span>Last Name</span>
                     <input type="text" placeholder="" name="lname">
                  </li>
                  <li>
                     <span>Parents name</span>
                     <input type="text" placeholder="" name="Parents">
                  </li>
                  <li>                     
                     <span>Contact No.</span>
                     <input type="text" placeholder="" name="Contact">
                  </li>
                  <li>                     
                     <span>Email ID</span>
                     <input type="text" placeholder="" name="Email">
                  </li>
                  <li>                     
                     <span>School Name</span>
                     <input type="text" placeholder="" name="School">
                  </li>
                  <li>                     
                     <span>City Name</span>
                      <select>
                     	<option>Select Country</option>
                     	<option>Ahmedabad</option>
                     	<option>Surat</option>
                     	<option>Vadodara</option>
                     	<option>Rajkot</option>
                     	<option>Bhavnagar</option>
                     </select>
                  </li>
                  <li>                     
                     <span>State Name</span>
                     <select>
                     	<option>Select State</option>
                     	<option>Andhra Pradesh</option>
                     	<option>Haryana</option>
                     	<option>Assam</option>
                     	<option>Gujarat</option>
                     	<option>Goa</option>
                     </select>
                  </li>
                  <li>                     
                     <span>Country Name</span>
                     <select>
                     	<option>Select Country</option>
                     	<option>india</option>
                     	<option>Afghanistan</option>
                     	<option>Indonesia</option>
                     	<option>Iran</option>
                     	<option>Japan</option>
                     </select>
                  </li>
                  <li>                     
                     <span>Address</span>
                     <textarea></textarea>
                  </li>
               </ul>
               </form>
               <div class="btn default">
                  <a href="javascript:void(0)">UPDATE PROFILE</a>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
@stop