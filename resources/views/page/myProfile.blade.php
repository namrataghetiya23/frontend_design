@extends('main')
@section('content')
<section class="contain_wapper" id="contain_wapper">
   <div class="home_wapper">
      <div class="inner-banner">
         <div class="container">
            <div class="banner-content">
               <h1>My Profile</h1>
            </div>
         </div>
      </div>
      <div class="user-profile">
         <div class="container">
            <div class="user-header">
               <img src="images/user-icon.png">
            </div>
            <div class="user-title">
               <span>StudentDetails</span>
            </div>
            <div class="user-datalist">
               <ul>
                  <li>
                     <strong>First Name:</strong>
                     <span>First Name</span>
                  </li>
                  <li>
                     <strong>Last Name:</strong>
                     <span>Last Name</span>
                  </li>
                  <li>
                     <strong>Parents name:</strong>
                     <span>Parents name</span>
                  </li>
                  <li>
                     <strong>Contact No.:</strong>
                     <span>Contact No.</span>
                  </li>
                  <li>
                     <strong>Email ID:</strong>
                     <span>Email ID</span>
                  </li>
                  <li>
                     <strong>School:</strong>
                     <span>School Name</span>
                  </li>
                  <li>
                     <strong>City:</strong>
                     <span>City Name</span>
                  </li>
                  <li>
                     <strong>Country:</strong>
                     <span>Country Name</span>
                  </li>
                  <li>
                     <strong>Country:</strong>
                     <span>Country Name</span>
                  </li>
                  <li>
                     <strong>Show Email Id/Phone Number:</strong>
                     <span>+91 7878 9999 1542</span>
                  </li>
                  <li>
                     <strong>Address:</strong>
                     <span>Address</span>
                  </li>
               </ul>
               <div class="btn default">
                  <a href="javascript:void(0)">EDIT PROFILE</a>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
@stop