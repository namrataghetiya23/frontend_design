@extends('main')
@section('content')

<section class="contain_wapper login-bg" id="contain_wapper">
	<div class="home_wapper">
		<div class="container">
			<form>
				<div class="title">
					<h2>Register</h2>
				</div>
				<div class="form-group">
					<!-- <label for="inputEmail4">Email</label> -->
					<select class="form-control">
						<option>Select User type *</option>
						<option>Student </option>
						<option>Tutor</option>
					</select>
				</div>
				<div class="form-group">
					<input type="email" class="form-control" id="inputEmail4" placeholder="Enter First Name *">
				</div>
				<div class="form-group">
					<input type="email" class="form-control" id="inputEmail41" placeholder="Enter Last Name *">
				</div>
				<div class="form-group">
					<input type="email" class="form-control" id="inputEmail42" placeholder="Enter Mobile Number *">
				</div>
				<div class="form-group">
					<input type="email" class="form-control" id="inputEmail43" placeholder="Enter Email ID *">
				</div>
				<div class="form-group">
					<input type="email" class="form-control" id="inputEmail44" placeholder="Enter Username *">
				</div>
				<div class="form-group">
					<!-- <label for="inputPassword4">Password</label> -->
					<input type="password" class="form-control" id="inputPassword4" placeholder="Enter Password *">
				</div>
				<div class="form-group">
					<!-- <label for="inputPassword4">Password</label> -->
					<input type="password" class="form-control" id="inputPassword41" placeholder="Enter Confirm Password *">
				</div>
				<div class="form-group">
					<input type="email" class="form-control" id="inputEmail45" placeholder="Enter School Name">
				</div>
				<div class="form-group">
					<!-- <input type="email" class="form-control" id="inputEmail46" placeholder="Upload Documents"> -->
					
					<div class="file-upload-wrapper" data-text="Select your file!">
						<input name="file-upload-field" type="file" class="file-upload-field" value="">
					</div>
				</div>
				<button type="submit" class="btn btn-primary">REGISTER NOW</button>
				<div class="signup-link">
					<span>You already have an accoun? <a href="javascript:void(0)" class="orange-text">Log in here</a></span>
				</div>
				<div class="divider">
					<span></span>
					<label>OR</label>
					<span></span>
				</div>
				<div class="singup-opction">
					<button class="btn google-btn"><i class="fa fa-google" aria-hidden="true"></i> Sign in With Google</button>
					<button class="btn fb-btn"><i class="fa fa-facebook-official" aria-hidden="true"></i> Sign in With Facebook</button>
					<button class="btn linkedin-btn"><i class="fa fa-linkedin-square" aria-hidden="true"></i> Sign in With Linkedin</button>
				</div>
			</form>
		</div>
	</div>
</section>

@stop