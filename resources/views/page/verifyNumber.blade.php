@extends('main')
@section('content')

<section class="contain_wapper login-bg pt-90 pb-90" id="contain_wapper">

	<div class="home_wapper">

		<div class="container">

			<form method="get" class="digit-group" data-group-name="digits" data-autosubmit="false" autocomplete="off">

				<div class="title text-left">

					<h2>Verify Number</h2>

					<p>Enter OTP that you received on  your Registered Number</p>

				</div>

				<input type="text" id="digit-1" name="digit-1" data-next="digit-2" />

				<input type="text" id="digit-2" name="digit-2" data-next="digit-3" data-previous="digit-1" />

				<input type="text" id="digit-3" name="digit-3" data-next="digit-4" data-previous="digit-2" />

				<span class="splitter">&ndash;</span>

				<input type="text" id="digit-4" name="digit-4" data-next="digit-5" data-previous="digit-3" />

				<input type="text" id="digit-5" name="digit-5" data-next="digit-6" data-previous="digit-4" />

				<input type="text" id="digit-6" name="digit-6" data-previous="digit-5" />

				<div class="signup-link text-left">

					<span>Not received OTP? <a href="javascript:void(0)" class="orange-text">Resend</a></span>

				</div>



				

				<button type="submit" class="btn btn-primary">SUBMIT</button>

				

			</form>

		</div>

	</div>

</section>
@stop