<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('login', 'ViewController@login')->name('login');
Route::get('myProfile', 'ViewController@myProfile')->name('myProfile');
Route::get('register', 'ViewController@register')->name('register');
Route::get('userType', 'ViewController@userType')->name('userType');
Route::get('verifyNumber', 'ViewController@verifyNumber')->name('verifyNumber');
Route::get('editProfile', 'ViewController@editProfile')->name('editProfile');
Route::get('BookingRequests', 'ViewController@BookingRequests')->name('BookingRequests');
Route::get('BookedCourses', 'ViewController@BookedCourses')->name('BookedCourses');
Route::get('TeacherProfile', 'ViewController@TeacherProfile')->name('TeacherProfile');
Route::get('PaymentHistory', 'ViewController@PaymentHistory')->name('PaymentHistory');

Route::get('SubjectDetail', 'ViewController@SubjectDetail')->name('SubjectDetail');
Route::get('SessionDetail', 'ViewController@SessionDetail')->name('SessionDetail');
Route::get('SessionDetail2', 'ViewController@SessionDetail2')->name('SessionDetail2'); 
Route::get('AccountSettings', 'ViewController@AccountSettings')->name('AccountSettings'); 